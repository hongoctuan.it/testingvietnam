<?php 
class M_booking extends CI_model
{
	//lay danh sach tat ca product
	public function getBooking()
	{
		$arr=array();
		$this->db->select('c.name as name,b.id, b.date, b.time, b.number, b.product, b.note, c.phone, p.name as product, b.active');
		$this->db->from('booking b');
		$this->db->join('customer c', 'b.customer_id = c.id');
		$this->db->join('product p', 'b.product = p.id');
		$this->db->order_by("c.name", "asc");
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

}
?>