<?php 
class M_search extends CI_model
{
	public function getSearch($table, $column, $value, $deleted=false){
		$this->db->like($column, $value);
		if($deleted){
			$this->db->where("deleted",0);
		}
		$query = $this->db->get($table);
		$this->db->order_by("id", "asc");
        foreach($query->result() as $row)
		{  
            $arr[]=$row;
		}
		return $arr;
	}
}
?>