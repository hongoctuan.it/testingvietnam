<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="ico" href="../statics/default/img/logo_small.png" />
    <title>Dolly spa quan tan phu</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <script type="text/javascript" src="<?php echo site_url('filemanager/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('filemanager/ckfinder/ckfinder.js'); ?>"></script>
<script>
    function search(){
        var value = document.getElementById("search-input").value;
        if(value!="")
        {
            
            var control = "<?php echo $this->uri->segment(2)?>";
            document.getElementById("search-input").value = "";
            window.location.href="<?php echo site_url('admin/search?control=');?>"+control+"&value="+value;
        }
    }
</script>
<link href="<?php echo site_url('statics/cms/main.css')?>" rel="stylesheet">
<link href="<?php echo site_url('statics/cms/style.css')?>" rel="stylesheet">
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src">
                </div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>    <div class="app-header__content">
                <div class="app-header-left">
                    <div class="search-wrapper">
                        <div class="input-holder">
                            <input type="text" class="search-input" id="search-input" placeholder="Type to search">
                            <button onclick="search()" class="search-icon"><span></span></button>
                        </div>
                        <button  class="close"></button>
                    </div>
                </div>
            </div>
        </div>        
            <div class="app-main">
                <div class="app-sidebar sidebar-shadow">
                    <div class="app-header__logo">
                        <div class="logo-src"></div>
                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                    </div>    <div class="scrollbar-sidebar">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <li class="app-sidebar__heading">Dịch Vụ</li>
                                <li>
                                    <a href="<?php echo site_url('admin/category')?>">
                                        <i class="metismenu-icon pe-7s-diamond"></i>
                                        Dịch vụ
                                        <i class="metismenu-state-icon caret-left"></i>
                                    </a>
                                </li>
                                <li  >
                                    <a href="<?php echo site_url('admin/gallery_detail')?>">
                                        <i class="metismenu-icon pe-7s-display2"></i>
                                        Khuyến mãi
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/booking')?>">
                                        <i class="metismenu-icon pe-7s-diamond"></i>
                                        Đặt chỗ
                                        <i class="metismenu-state-icon caret-left"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/order')?>">
                                        <i class="metismenu-icon pe-7s-diamond"></i>
                                        Đơn hàng
                                        <i class="metismenu-state-icon caret-left"></i>
                                    </a>
                                </li>
                                
                                <li class="app-sidebar__heading">Thông tin Chung</li>
                                <li>
                                    <a href="<?php echo site_url('admin/companyinfo')?>">
                                        <i class="metismenu-icon pe-7s-mouse">
                                        </i>Thông tin spa
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/news')?>">
                                        <i class="metismenu-icon pe-7s-car"></i>
                                            Giới thiệu
                                        <i class="metismenu-state-ico caret-left"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/banner')?>">
                                        <i class="metismenu-icon pe-7s-eyedropper">
                                        </i>Banner
                                    </a>
                                </li>
                                <li class="app-sidebar__heading">Thống Kê</li>
                                <li>
                                    <a href="<?php echo site_url('admin/analytic')?>">
                                        <i class="metismenu-icon pe-7s-graph2">
                                        </i>Thống kê
                                    </a>
                                </li>
                                <li class="app-sidebar__heading">Tài Khoản</li>
                                <li>
                                    <a href="<?php echo site_url('logout')?>">
                                        <i class="metismenu-icon pe-7s-user">
                                        </i>Đăng xuất
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>    
                <div class="app-main__outer">

                    