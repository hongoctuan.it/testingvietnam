<?php
	$action = $obj ? site_url('admin/category?act=upd&id=' . $obj->id . "&token=" . $infoLog->token) : site_url('admin/category?act=upd&token=' . $infoLog->token);
?>
<div class="app-main__inner">
	<div class="app-page-title">
		<div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon">
					<img src="<?php echo site_url('statics/default/img/logo_small.png');?>" width="100%"/>
				</div>
				<div>Chi tiết khách hàng đăng kí đặt chỗ</div>
			</div>
			<div class="page-title-actions">
				<button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
					<i class="fa fa-star"></i>
				</button>
				<div class="d-inline-block dropdown">
					<a href="<?php echo site_url('admin/category?act=upd&token='.$infoLog->token)?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-info">
						
							<span class="btn-icon-wrapper pr-2 opacity-7">
								<i class="fa fa-business-time fa-w-20"></i>
							</span>
							Thêm Dịch Vụ
						</button>
					</a>
				</div>
			</div>    
		</div>
	</div>            
	<div class="main-card mb-3 card">
		<div class="card-body">
			<h5 class="card-title">Bootstrap 4 Form Validation</h5>
			<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Tên Dịch Vụ</label>
						<input type="text" class="form-control" name="name" id="validationCustom01" placeholder="Tên gói chăm sóc" value="<?php echo $obj?$obj->name:"";?>" required>
					</div>
					<div class="col-md-6 mb-3">
						<label for="exampleText" class="">Mô Tả Ngắn</label>
						<textarea name="description" id="exampleText" class="form-control"><?php echo $obj ? $obj->description : ""; ?></textarea>
					</div>
				</div>
				<a class="btn btn-default" href="<?php echo site_url('admin/category'); ?>">Quay lại</a>
				<button type="reset" class="btn btn-warning">Huỷ</button>
				<button type="submit" id="formSubmit" class="btn btn-primary">Lưu Lại</button>			
			<?php echo form_close(); ?>
		</div>
	</div>
</div>