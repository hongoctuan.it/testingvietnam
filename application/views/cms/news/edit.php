<?php
	$action = $obj?site_url('admin/news?act=upd&id='.$obj->id."&token=".$infoLog->token):site_url('admin/news?act=upd&token='.$infoLog->token);
	$image_01 = $obj&&$obj->img!=""?base_url('assets/public/avatar/'.$obj->img):base_url('assets/public/avatar/no-avatar.png');

?>

<div class="app-main__inner">       
	<div class="main-card mb-3 card">
		<div class="card-body">
			<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Cập nhật thông tin giới thiệu</label>
						<input type="text" class="form-control" name="name" id="validationCustom01" placeholder="Tên tin tức" required value="<?php echo $obj?$obj->name:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label>Hình ảnh</label>
						<div>
							<img id="imgFile_01" class="imgFile" alt="Avatar" src="<?php echo $image_01?>" width="300px" height="200px"/>
							<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
						</div>
					</div>
					<div class="col-md-12 mb-3">
						<label for="validationCustom01">Mô tả</label>
						<textarea id="description" class="form-control" name="description" required id="description" ><?php echo $obj?$obj->description:"";?></textarea>
						<script>
									var editor = CKEDITOR.replace('description',{
										language:'vi',
										filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

										filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
										
										filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
										
										filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
										
										filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
										
										filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

									});
                        		</script>
					</div>
				</div>

				<a class="btn btn-default" href="<?php echo site_url('admin/news'); ?>">Quay lại</a>
				<button type="reset" class="btn btn-warning">Huỷ</button>
				<button type="submit" id="formSubmit" class="btn btn-primary">Lưu Lại</button>			
			<?php echo form_close(); ?>
		</div>
	</div>
</div>