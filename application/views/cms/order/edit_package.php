<?php
	$action = $obj ? site_url('admin/order?act=upd&id=' . $obj->id . "&token=" . $infoLog->token) : site_url('admin/order?act=upd&token=' . $infoLog->token);
?>
<?php $this->load->view('cms/_layout/datetime'); ?>

<div class="app-main__inner">           
	<div class="main-card mb-3 card">
		<div class="card-body">
			<h5 class="card-title">Chi tiết gói đã mua</h5>
			<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Tên Khách</label>
						<input type="text" class="form-control" name="name" id="validationCustom01" placeholder="Tên khách hàng" value="<?php echo $obj?$obj->name:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Số điện thoại</label>
						<input type="text" class="form-control" name="phone" id="validationCustom01" placeholder="Số điện thoại" value="<?php echo $obj?$obj->phone:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Gói </label>
						<select name="product_id" id="exampleSelect" class="form-control">
							<?php foreach($products as $product):?>
								<option value="<?php echo $product?$product->id:"";?>">
									<?php echo $product?$product->name:"";?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Số lần </label>
						<input type="text" class="form-control" name="times" id="validationCustom01" placeholder="Số lần" value="<?php echo $obj?$obj->times:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Hạn dùng</label>
						<input type="text" class="form-control" name="end_date" id="datepicker" placeholder="Ngày hết hạn" value="<?php echo $obj?date("d-m-Y",$obj->end_date):"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Thanh toán</label>
						<input type="text" class="form-control" name="paid" id="validationCustom01" placeholder="Thanh toán" value="<?php echo $obj?$obj->paid:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Còn lại</label>
						<input type="text" class="form-control" name="debt" id="validationCustom01" placeholder="Còn lại" value="<?php echo $obj?$obj->debt:"";?>" required>
					</div>
					<div class="col-md-6 mb-3">
						<label for="exampleText" class="">Ghi chú</label>
						<textarea name="note" id="exampleText" class="form-control"><?php echo $obj ? $obj->note : ""; ?></textarea>
					</div>
				</div>
				<a class="btn btn-default" href="<?php echo site_url('admin/order'); ?>">Quay lại</a>
				<button type="reset" class="btn btn-warning">Huỷ</button>
				<button type="submit" id="formSubmit" class="btn btn-primary">Lưu Lại</button>
			<?php echo form_close(); ?>
		</div>
	</div>

	<div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">

					<?php if($obj->times>sizeof($obj_details) && $obj->end_date > time()):?>
						<a href="#" title="Delete" onclick="btnaddordertimes('<?php echo $infoLog->token?>')" data-toggle="modal" data-target="#addordertimesModal">
							<button type="button" class="btn-shadow dropdown-toggle btn btn-info" style="margin-bottom:20px">
								<span class="btn-icon-wrapper pr-2 opacity-7">
									<i class="fa fa-business-time fa-w-20"></i>
								</span>
								Thêm Lần
							</button>
						</a>
					<?php endif;?>
                    <table class="mb-0 table table-striped">
                        <thead>
                        <tr>
                            <th>Lần</th>
							<th>Ngày làm</th>
							<th>Ghi chú</th>
                        </tr>
                        </thead>
                        <tbody>
							<?php if($obj_details):
								foreach($obj_details as $key=>$obj_detail){
							?>
								<tr>
									<th scope="row"><?php echo $key+1?></th>
									<td>
										<?php echo $obj_detail->date?>
									</td>
									<td>
										<?php echo $obj_detail->note?>
									</td>
								</tr>
							<?php } endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>