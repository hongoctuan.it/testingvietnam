<div class="banner">
    <h3>DỊCH VỤ</h3>
</div>
<section class="category-product-list-v2">
    <div class="bg-white service_content" style="padding-top:15px">
        <div class="category-product-list-filter">
            <div class="row">
                <div class="col-md-12" style="text-align: center">
                    <?php if (isset($cats)) : ?>
                        <?php foreach ($cats as $category) : ?>
                        <?php $name = str_replace('Nhật Bản','',$category->name);?>
                            <?php if ($category->level == 0):?>
                            <a href="#<?php echo $category->slug?>" class="category-filter-button"><?php echo $name;?></a><?php endif;?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                </div>
                <div class="col-12">
                        <?php foreach ($cats as $key => $cat) : ?>
                            <div class="row">
                                <?php if (isset($cat->products) && count($cat->products) > 0) : ?>
                                    <h3 class="col-12 title" id="<?php echo $cat->slug?>">
                                        <?php echo $cat->name?>
                                    </h3>
                                    <br>
                                    <?php foreach ($cat->products as $product) : ?>
                                        <div class="product col-lg-6 col-md-12">
                                            <p class="name"><?php echo $product->name ?></p>
                                            <p class="separator_dots"></p>
                                            <p class="price"><?php echo number_format($product->price, 0, ',', '.') ?>/<?php echo $product->time ?>'</p>
                                        </div>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                    <div class="col-12 col-md-auto">
                                        <p>Chưa có sản phẩm</p>
                                    </div>
                                
                                <?php endif; ?>
                            </div>
                        <?php endforeach;?>
                    
                </div>
            </div>
        </div>
    </div>
</section>
