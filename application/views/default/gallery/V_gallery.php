<div class="banner">
    <h3>KHUYẾN MÃI</h3>
</div>
<section class="category-product-list-v2">
    <div class="bg-white service_content" style="padding-top:15px">
        <div class="category-product-list-filter">
            <div class="row" style="margin:auto">
                <div class="col-12" style="text-align:center">
                    <?php foreach ($gallery_datas as $gallery_data) : ?>
                            <a href="<?php echo site_url("khuyen-mai/".$gallery_data->id."/".$gallery_data->slug); ?>"><div class="promotion col-lg-4 col-md-12">
                                <img class="event_image" src="<?php echo base_url(); ?>assets/public/avatar/<?php echo $gallery_data->img;?>"/>
                                <div class="title"><?php echo $gallery_data ? $gallery_data->name : ""; ?></div>
                            </div>
                            </a>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
