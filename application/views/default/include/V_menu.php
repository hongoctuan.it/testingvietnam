<header>
        <nav class="navbar-expand-lg navbar-dark navbar-lg" id="navbartop">
            <div class="container navbar-desktop brand-bar">
                <a class="navbar-brand brand-md my-0 text-white text-center" href="#">
                    <p class="brand-logo">NguyenQuanCo</p>
                </a>

                <p class="brand-slogan navbar-brand brand-md mr-auto text-white text-center">Slogan</p>

                <form class="form-inline ml-auto my-lg-0">
                    <input class="form-control mr-sm-2 search-input" type="search" placeholder="Từ khoá tìm kiếm">
                    <!-- <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> -->
                    <button type="submit" class="search-submit-button">
                        <i class="fas fa-search"></i>
                    </button>
                </form>
            </div>
            <div class="container menu-bar">
                <ul class="navbar-nav navbar-nav-lg">
                    <li class="nav-item ">
                        <a class="nav-link mx-2 text-white" href="<?php echo site_url();?>">Trang Chủ
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link mx-2 text-white" href="category">Sản Phẩm</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link mx-2 text-white" href="about">Về Chúng Tôi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mx-2 text-white" href="partner">Đối Tác</a>
                    </li>
                </ul>
            </div>
        </nav>
        <nav class="navbar navbar-expand-lg navbar-dark navbar-mobile">
            <div class="container">
                <a class="navbar-brand brand-sm text-white" href="#">NguyenQuanCo
                    <span class="text-white mr-auto ml-2 small">slogan</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item ">
                            <a class="nav-link text-white" href="<?php echo site_url();?>">Trang Chủ
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="category">Sản Phẩm</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-white" href="about">Về Chúng Tôi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="partner">Đối Tác</a>
                        </li>
                    </ul>

                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2 search-input" type="search" placeholder="Từ khoá tìm kiếm">
                        <!-- <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> -->
                        <button type="submit" class="search-submit-button">
                            <i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>
            </div>
        </nav>
    </header>