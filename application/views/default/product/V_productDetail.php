<section class="product-overview py-4">
    <div class="container">
        <div class="row text-secondary bread-crumb my-4">
            <div class="col-12">
                <a href="<?php echo site_url(); ?>" style="text-decoration:none;color: #6c757d !important;">Home</a> / <a href="<?php echo site_url(); ?>danh-muc-san-pham" style="text-decoration:none;color: #6c757d !important;">Sản Phẩm</a> / <a href="<?php echo site_url($product->category_slug); ?>" style="text-decoration:none;color: #6c757d !important;"><?php echo $product->category_name; ?></a> /
                <b class="text-active"><?php echo $product->name; ?> </b>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-12 product-overview-images">

                <div class="swiper-container gallery-top">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo isset($product->img1)?'assets/public/avatar/' . $product->img1:''; ?>')">
                        </div>
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo isset($product->img2)?'assets/public/avatar/' . $product->img2:''; ?>')">
                        </div>
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo isset($product->img3)?'assets/public/avatar/' . $product->img3:''; ?>')">
                        </div>
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo isset($product->img4)?'assets/public/avatar/' . $product->img4:''; ?>')">
                        </div>
                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next swiper-button-black"></div>
                    <div class="swiper-button-prev swiper-button-black"></div>
                </div>
                <div class="swiper-container gallery-thumbs">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo isset($product->img1)?'assets/public/avatar/' . $product->img1:''; ?>')">
                        </div>
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo isset($product->img2)?'assets/public/avatar/' . $product->img2:''; ?>')">
                        </div>
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo isset($product->img3)?'assets/public/avatar/' . $product->img3:''; ?>')">
                        </div>
                        <div class="swiper-slide" style="background-image:url('<?php echo base_url(); ?><?php echo isset($product->img4)?'assets/public/avatar/' . $product->img4:''; ?>')">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 product-overview-info mt-5">
                <div class=" text-center" style="padding:0px">
                    <h1 class="product-name my-2"><?php echo $product->name; ?></h1>

                    <?php if ($product->price != 0) : ?>
                        <p class='' style="font-size:20px;font-weight:500;color:orange"><?php echo number_format($product->price, 0, ',', '.'); ?> VND</p>
                    <?php endif; ?>
                    <p style="margin-bottom:5px"><button class="share-fb btn" onclick="share_fb_function()" style="position:relative!important;z-index:10;background:#3b5998;font-size:14px;color:white;padding:0px 5px ;line-height:20px;height:20px;bottom:0px;left:0px;"><i class="fab fa-facebook"></i> <span style="line-height:20px;font-size:11px;margin-bottom:5px;font-weight:400;margin-bottom:2px;margin-left:3px"> Chia Sẻ</span></button></p>
                    <div class="zalo-share-button" data-href="<?php echo site_url('san-pham/' . $product->slug); ?>" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false style=""></div>
                    <?php if ($product->youtube) : ?>
                        <iframe width="100%" height="300" src="https://www.youtube.com/embed/<?php echo $product->youtube ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                        </iframe>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="product-details mb-1">
    <div class="container">
        <ul class="nav nav-tabs text-center justify-content-center" id="myTab" role="tablist">
            <li class="nav-item mx-3">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#product-details-desc" role="tab" aria-controls="product-details-desc" aria-selected="true">Mô Tả Chi Tiết</a>
            </li>
            <li class="nav-item mx-3">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#product-details-brand-info" role="tab" aria-controls="product-details-brand-info" aria-selected="false">Thông Tin Hãng Sản Xuất</a>
            </li>
            <li class="nav-item mx-3">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#product-details-question" role="tab" aria-controls="product-details-question" aria-selected="false">Hỏi Đáp</a>
            </li>
            <li class="nav-item mx-3">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#product-details-partner" role="tab" aria-controls="product-details-partner" aria-selected="false">Cửa Hàng Đang Bày Bán SP</a>
            </li>
        </ul>
        <div class="tab-content text-center py-2" id="myTabContent">
            <div class="tab-pane fade show active" id="product-details-desc" role="tabpanel" aria-labelledby="product-details-desc-tab">
                <?php echo $product->detail_des; ?>
            </div>

            <div class="tab-pane fade" id="product-details-brand-info" role="tabpanel" aria-labelledby="product-details-brand-info-tab">
                Chưa có thông tin
            </div>

            <div class="tab-pane fade" id="product-details-question" role="tabpanel" aria-labelledby="product-details-question-tab">
                <div class="row text-left">
                    <div class="col-md-12">


                    </div>
                    <div class="col-md-12 product-details-question-input">
                        <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-numposts="5"></div>

                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="product-details-partner" role="tabpanel" aria-labelledby="product-details-partner">
                <?php if (isset($partner)) : ?>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="row shops-mien">
                                <div class="col-md-12 shops-list">
                                    <?php foreach ($partner as $item) : ?>
                                        <div class="shops-wrap shops-show-default">
                                            <h3 class="shops-title text-center mt-2" style="cursor:pointer"><?php echo $item->name; ?></h3>
                                            <p class="shops-address">Địa chỉ: <?php echo $item->address; ?>
                                                <br>Số ĐT: <?php echo $item->phone; ?></p>
                                            <input type=hidden value="<?php echo $item->map_src; ?>" class="map_src">
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 google-map-iframe-wrap">
                            <iframe class="google-map-iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62694.93338363085!2d106.71814491147437!3d10.854608470658878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d85e042bf04b%3A0xbb26baec1664394d!2zVGjhu6cgxJDhu6ljLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1555423397586!5m2!1svi!2s" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                <?php else : ?>
                    <p>Không có danh sách cửa hàng</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<section class="home-featured-products">
    <div class="container">
        <div class="row section-title-wrap mb-4">
            <div class="section-title-line"></div>
            <h2 class="text-center section-title m-auto px-3">Sản phẩm liên quan</h2>
        </div>
    </div>
    <div class="container p-5">
        <div class="owl-carousel featured-products-slide">
            <?php if ($relate_products) : ?>
                <?php foreach ($relate_products as $relate_product) : ?>
                    <?php if ($relate_product->id != $product->id) : ?>
                        <div class="justify-content-center">
                            <figure class="figure featured-products-item-figure">
                                <a href="<?php echo site_url('san-pham/' . $relate_product->slug); ?>" class="featured-products-link">
                                    <div class="featured-products-item-wrap">
                                        <div class="featured-products-img-wrap">
                                        <!-- <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div> -->
                                            <img class="featured-products-img img-fluid owl-lazy" data-src="<?php echo base_url(); ?>assets/public_thumbs/avatar/<?php echo $relate_product->img1 ?>" alt="<?php echo $relate_product->short_des ?>">
                                        </div>
                                        <figcaption class="figure-caption m-auto">
                                            <h3 class="featured-products-title text-center text-dark mt-2">
                                                <?php echo $relate_product->name ?></h3>
                                        </figcaption>
                                    </div>
                                </a>
                            </figure>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
